import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.model.dataformat.JsonLibrary;

import java.util.ArrayList;

public class PokedexCamel {

    public static void main(String[] args) throws Exception {
        CamelContext camelContext = new DefaultCamelContext();

        camelContext.addRoutes(new RouteBuilder() {
            public void configure() throws Exception {
                from("file:input-pokemon?noop=true").
                        routeId("rota-principal").
                        split().xpath("/pokemons/name").
                        to("direct:rota-pokeapi").
                        to("direct:rota-pokedex");

                from("direct:rota-pokeapi").
                        routeId("rota-pokeapi").
                        setProperty("pokemonName", xpath("/name/text()")).
                        log("Buscando dados do Pokemon: ${property.pokemonName}").
                        setHeader(Exchange.HTTP_METHOD, HttpMethods.GET).
                        setHeader(Exchange.HTTP_PATH, simple("${property.pokemonName}")).
                        to("http4://pokeapi.co/api/v2/pokemon");

                String queryParameters = "authMethod=Basic&authPassword=admin&authUsername=admin";

                from("direct:rota-pokedex")
                        .routeId("rota-pokedex")
                        .log("Salvando dados do Pokemon na Pokedex")
                        .setHeader(Exchange.HTTP_METHOD, HttpMethods.POST)
                        .setHeader(Exchange.CONTENT_TYPE, simple("application/json"))
                        .setHeader(Exchange.HTTP_PATH, constant("pokedex"))
                        .process(exchange -> {
                            exchange.getIn().setHeader("chave", simple("chave-01"));

                        })
                        .to("http4://localhost:8080")
                        .log("Body: ${bodyAs(String)}")
                        .log("${header.chave}");
            }
        });

        // .to("http4://localhost:8081") nao existe
        // Connection refused: connect

        // retornando erro
        // HTTP operation failed invoking http://localhost:8080/pokedex with statusCode: 500


        camelContext.start();
        Thread.sleep(20000);
        camelContext.stop();
    }
}
